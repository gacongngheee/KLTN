<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Congrate!!!!</h2>

        <div>
            <p>Chúc mừng bạn đã mua hàng thành công tại Bstore. chúng tôi sẽ chuyển hàng cho bạn sớm nhất có thể <br>
            bằng các thông tin về địa chỉ, số điện thoại, email mà bạn đã cung cấp.<br> 
            Dưới đây là đơn hàng của bạn <br></p>

            <table border="1" borderColor="steelblue" style = "BORDER-COLLAPSE:collape;" align = "center">
                <tr bgcolor = "#00ffff">
                    <td>Product</td>
                    <td>Quantity</td>
                    <td>Unit Price</td>
                    <td>Cost</td>
                </tr>
                @foreach($content as $item)
                <tr>
                    <td> {{$item->name}}</td>
                    <td> {{$item->qty}}</td>
                    <td> {{ number_format($item->price,0,",",".")}} VNĐ</td>
                    <td> {{ number_format($item->price*$item->qty,0,",",".")}} VNĐ</td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td> {{ number_format($total,0,",",".")}} VNĐ</td>      
                </tr>
            </table>

        </div>

        <div> 
            <p>hãy click vào link dưới đây để xác thực đơn hàng của bạn. <br>
            {{ url('order/verify/' . $key_active) }}. </p>

        </div>

    </body>
</html>