<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
    
<!-- Nulled by http://www.baobinh.net by tieulonglanh -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Welcome to b.store</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png') }}">
		
		<!-- FONTS
		============================================ -->	
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'> 
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Bitter:400,700,400italic&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">			
		
		<!-- FANCYBOX CSS
		============================================ -->			
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">	
		
		<!-- BXSLIDER CSS
		============================================ -->			
        <link rel="stylesheet" href="{{ asset('css/jquery.bxslider.css') }}">			
				
		<!-- MEANMENU CSS
		============================================ -->			
        <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">	
		
		<!-- JQUERY-UI-SLIDER CSS
		============================================ -->			
        <link rel="stylesheet" href="{{ asset('css/jquery-ui-slider.css') }}">		
		
		<!-- NIVO SLIDER CSS
		============================================ -->			
        <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}">
		
		<!-- OWL CAROUSEL CSS 	
		============================================ -->	
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
		
		<!-- OWL CAROUSEL THEME CSS 	
		============================================ -->	
         <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
		
		<!-- BOOTSTRAP CSS 
		============================================ -->	
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		
		<!-- FONT AWESOME CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
		
		<!-- NORMALIZE CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
		
		<!-- MAIN CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
		
		<!-- STYLE CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('style.css') }}">
		
		<!-- RESPONSIVE CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
		
		<!-- IE CSS 
		============================================ -->
        <link rel="stylesheet" href="{{ asset('css/ie.css') }}">		
		
		<!-- MODERNIZR JS 
		============================================ -->
        <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <style>
			.img_cart{width: 70px;height: 70px; margin}
			.img_bestseller{width: 80; height: 106; margin }
		</style>
    </head>
    <body class="index-2">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		
		<!-- HEADER-TOP START -->
		<div class="header-top">
			<div class="container">
				<div class="row">
					<!-- HEADER-LEFT-MENU START -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="header-left-menu">
							<div class="welcome-info">
								Welcome <span>BootExperts</span>
							</div>
						</div>
					</div>
					<!-- HEADER-LEFT-MENU END -->
					<!-- HEADER-RIGHT-MENU START -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="header-right-menu">
							<nav>
								<ul class="list-inline">
									<li><a href="{{url('checkout')}}">Check Out</a></li>
									<li><a href="{{url('yourcart') }}">My Cart</a></li>
								</ul>									
							</nav>
						</div>
					</div>
					<!-- HEADER-RIGHT-MENU END -->
				</div>
			</div>
		</div>
		<!-- HEADER-TOP END -->
		<!-- HEADER-MIDDLE START -->
		<section class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- LOGO START --> 
						<div class="logo">
							<a href = "{{url('home')}}"><img src="{{asset ('img/logo2.png') }}" alt="bstore logo" /></a>
						</div>
						<!-- LOGO END -->
						<!-- HEADER-RIGHT-CALLUS START -->
						<div class="header-right-callus">
							<h3>call us free</h3>
							<span>0123-456-789</span>
						</div>
						<!-- HEADER-RIGHT-CALLUS END -->
						<!-- CATEGORYS-PRODUCT-SEARCH START -->
						<div class="categorys-product-search">
							<form method="GET" action="{{url('/SearchByCate')}}" enctype= "multipart/form-data">
								<div class="search-product form-group">
									<select name="cateId" class="cat-search">
										<option value = "0">Categories</option>
										<?php App\Libraries\MyFunction::cate_parent($category); ?>
									</select>
									<input type="text" class="form-control search-form" name="searchcate" placeholder="Enter your search key ... " />
									<button class="search-button" value="Search" name="s" type="submit">
										<i class="fa fa-search"></i>
									</button>									 
								</div>
							</form>
						</div>
						<!-- CATEGORYS-PRODUCT-SEARCH END -->
					</div>
				</div>
			</div>
		</section>
		<!-- HEADER-MIDDLE END -->
		<!-- MAIN-MENU-AREA START -->
		<header class="main-menu-area">
			<div class="container">
				<div class="row">
					<!-- SHOPPING-CART START -->
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right shopingcartarea">
						<div class="shopping-cart-out pull-right">
							<div class="shopping-cart">
								<a class="shop-link" href="cart.html" title="View my shopping cart">
									<i class="fa fa-shopping-cart cart-icon"></i>
									<b>My Cart</b>
									<span class="ajax-cart-quantity">{{$count}}</span> <!--tong san pham-->
								</a>
								<div class="shipping-cart-overly">
								@foreach($content as $item)
									<div class="shipping-item">
										<span class="cross-icon"><a href="{{ url('deleteCart',['id'=>$item->rowid]) }}" class="fa fa-times-circle"></a></span>
										<div class="shipping-item-image">
											<a href="#"><img src="{{ asset('img/imageProduct/'.$item->options->img) }}" alt="shopping image" class = "img_cart" /></a>
										</div>
										<div class="shipping-item-text">
											<span>{{$item->qty}}<span class="pro-quan-x">x</span> <a href="{{url('ProductDetail',$item->id)}}" class="pro-cat">{{$item->name}}</a></span>
											<p>{{ number_format($item->price,0,",",".")}} VNĐ</p>
										</div>
									</div>
								@endforeach
									<div class="shipping-total-bill">
										<div class="total-shipping-prices">
											<span class="shipping-total">{{ number_format($total,0,",",".")}} VNĐ</span>
											<span>Total</span>
										</div>										
									</div>
									<div class="shipping-checkout-btn">
										<a href="{{url('yourcart')}}" >Check out <i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<!-- SHOPPING-CART END -->
					<!-- MAINMENU START -->
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 no-padding-right menuarea">
						<div class="mainmenu">
							<nav>
								<ul class="list-inline mega-menu">
									<li class="active"><a href="index.html">Home</a>
									</li>
									<li>
										<a href="#">Sale</a>	
									</li>
									<li>
										<a href="#">Tops</a>
									</li>
									<li>
										<a href="#">Blogs</a>			
									</li>
									<li>
										<a href="#">News</a>
									<li>
										<a href="#">Events</a>
									</li>
									<li><a href="#">About us</a></li>
								</ul>
							</nav>
						</div>
					</div>
					<!-- MAINMENU END -->
				</div>			
			</div>
		</header>
		<!-- MAIN-MENU-AREA END -->
		<!-- HEADER-BOTTOM-AREA START -->
		<section class="header-bottom-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<!-- LEFT-CATEGORY-MENU START -->
						<div class="left-category-menu">
							<div class="left-product-cat">
								<div class="category-heading">
									<h2>category</h2>
								</div>
								<!-- CATEGORY-MENU-LIST START -->
								<div class="category-menu-list">
									<ul>
                   					<?php 
                   						$menu_level_1 = DB::table('category')->where('parentId', 0)->get();
                   					?>
                   					@foreach($menu_level_1 as $item_level_1)
                     					<li>
                      					<a href= "{{URL('loai-san-pham',[$item_level_1->id]) }}"> {{ $item_level_1->name }} </a>
                     					<div class="cat-left-drop-menu">
                      					<ul>
                      						<?php
                      						$menu_level_2 = DB::table('category')->where('parentId', $item_level_1->id)->get();
	                      					?>

	                      					@foreach($menu_level_2 as $item_level_2)
	                      					<li><a href = "{{URL('loai-san-pham',[$item_level_2->id]) }}"> {{ $item_level_2->name }} </a></li>
	                      					@endforeach
	                    				</ul>
	                  					</div>
	                  					</li>
	                  				@endforeach
                  				</ul>
								</div>
								<!-- CATEGORY-MENU-LIST END -->
							</div>
						</div>	
						<!-- LEFT-CATEGORY-MENU END -->			
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<!-- MAIN-SLIDER-AREA START -->
						<div class="main-slider-area">
							<div class="slider-area">
								<div id="wrapper">
									<div class="slider-wrapper">
										<div id="mainSlider" class="nivoSlider">
											<img src="{{asset ('img/slider/homepage2/3.jpg') }}" alt="main slider" title="#htmlcaption"/>
											<img src="{{asset ('img/slider/homepage2/4.jpg') }}" alt="main slider" title="#htmlcaption2"/>
										</div>
										<div id="htmlcaption" class="nivo-html-caption slider-caption">
											<div class="slider-progress"></div>
											<div class="slider-cap-text slider-text1">
												<div class="d-table-cell">
													<h2 class="animated bounceInDown">BIG SALE</h2>
													<p class="animated bounceInUp">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod ut laoreet dolore magna aliquam erat volutpat.</p>	
													<a class="wow zoomInDown" data-wow-duration="1s" data-wow-delay="1s" href="#">Detail <i class="fa fa-caret-right"></i></a>													
												</div>
											</div>	
										</div>
										<div id="htmlcaption2" class="nivo-html-caption">
											<div class="slider-progress"></div>
											<div class="slider-cap-text slider-text2">
												<div class="d-table-cell">
													<h2 class="animated bounceInDown">BIG SALE</h2>
													<p class="animated bounceInUp">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod ut laoreet dolore magna aliquam erat volutpat.</p>	
													<a class="wow zoomInDown" data-wow-duration="1s" data-wow-delay="1s" href="#">Detail <i class="fa fa-caret-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>								
							</div>											
						</div>	
						<!-- MAIN-SLIDER-AREA END -->
					</div>						
				</div>
			</div>
		</section>
		<!-- HEADER-BOTTOM-AREA END -->
		<!-- MAIN-CONTENT-SECTION START -->
		<section class="main-content-section">
			<div class="container">
				<div class="row">
					<!-- LEFT-SIDEBAR START -->
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<!-- SIDEBAR-LEFT-ADD START -->
						<div class="single-left-sidebar sidebar-left-add">
							<div class="sidebar-left zoom-img">
								<a href="#"><img src="{{ asset ('img/product/cms11.jpg') }}" alt="sidebar left" /></a>
							</div>	
						</div>
						<!-- SIDEBAR-LEFT-ADD END -->
						<!-- LEFT SIDEBAR-BEST-SELLER START -->
						<div class="single-left-sidebar sidebar-best-seller">
							<div class="left-title-area">
								<h2 class="left-title">bestseller</h2>
							</div>
							<div class="row">
								<!-- SIDEBAR-BEST-SELLER-CAROUSEL START -->	
								<div class="sidebar-best-seller-carousel">
									<!-- SIDEBAR-BEST-SELLER SINGLE ITEM START -->
									@foreach($bestproduct as $best)
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->
										<div class="single-product-item">
											<div class="sidebar-product-image">
												<a href="{{url('ProductDetail',$best->id)}}"><img src="{{asset ('img/imageProduct/'.$best->image) }}"  alt="product-image" /></a>
											</div>
											<?php 
     											if($best->sale == 1){
     												$sale1 = DB::table('sale')->select('todate')->where('product_id',$best->id)->first();
     												$datenow=date("Y-m-d");
     												if($sale1->todate < $datenow ){
     													echo "<a href='' class='new-mark-box'>NEW</a>";
     												}else{
     													echo "<a href='' class='new-mark-box'>SALE</a>";
     												}
     											}else{
     												echo "<a href='' class='new-mark-box'>NEW</a>";
     											}

     											?>
											<div class="product-info">
												<a href="{{url('ProductDetail',$best->id)}}">{{$best->name}}</a>
												<div class="price-box">
													<?php 
														if($best->sale == 1){
     														$sale1 = DB::table('sale')->select('todate','percent')->where('product_id',$best->id)->first();
     														$datenow=date("Y-m-d");
     														if($sale1->todate < $datenow ){
     															echo "<span class='price'>". number_format($best->cost,0,",",".")."VNĐ"."</span>";
     														}else{
     															echo "<span class='price'>". number_format(($best->cost-($best->cost*$sale1->percent/100)),0,",",".")."VNĐ"."</span>";
     															echo "<span class='old-price'>". number_format($best->cost,0,",",".")."VNĐ"."</span>";

     														}
     													}else{
     														echo "<a href='' class='new-mark-box'>NEW</a>";
     													}
													?>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-best END -->
									</div>
									<!-- SIDEBAR-BEST-SELLER SINGLE ITEM END -->
								@endforeach
								</div>	
								<!-- SIDEBAR-BEST-SELLER-CAROUSEL END -->	
							</div>
						</div>
						<!-- LEFT SIDEBAR-BEST-SELLER END -->
					</div>	
					<!-- LEFT-SIDEBAR END -->
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="new-product-area">
									<div class="left-title-area">
										<h2 class="left-title">New Products</h2>
									</div>						
									<div class="row">
										<div class="col-xs-12">
											<div class="row">
												<!-- HOME2-NEW-PRO-CAROUSEL START -->
												<div class="home2-new-pro-carousel">
													<!-- NEW-PRODUCT SINGLE ITEM START -->
													@foreach($newProducts as $item)
													<div class="item">
														<div class="new-product">
															<div class="single-product-item">
																<div class="product-image">
																	<a href="{{url('ProductDetail',$item->id)}}"><img src="{{asset ('img/imageProduct/'.$item->image) }}" alt="product-image" /></a>
																	<?php 
     																	if($item->sale == 1){
     																		$sale1 = DB::table('sale')->select('todate')->where('product_id',$item->id)->first();
     																		$datenow=date("Y-m-d");
     																		if($sale1->todate < $datenow ){
     																			echo "<a href='' class='new-mark-box'>NEW</a>";
     																		}else{
     																			echo "<a href='' class='new-mark-box'>SALE</a>";
     																		}
     																	}else{
     																		echo "<a href='' class='new-mark-box'>NEW</a>";
     																	}

     																	?>
																	<div class="overlay-content">
																		<ul>
																			<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
																			<li><a href="{{url('mua-hang',$item->id)}}" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
																			<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
																		</ul>
																	</div>
																</div>
																<div class="product-info">
																	<a "{{url('ProductDetail',$item->id)}}">{{$item->name}}</a>
																	<div class="price-box">
																		<?php 
																		if($item->sale == 1){
     																		$sale1 = DB::table('sale')->select('todate','percent')->where('product_id',$item->id)->first();
     																		$datenow=date("Y-m-d");
     																		if($sale1->todate < $datenow ){
     																			echo "<span class='price'>". number_format($item->cost,0,",",".")."VNĐ"."</span>";
     																		}else{
     																			echo "<span class='price'>". number_format(($item->cost-($item->cost*$sale1->percent/100)),0,",",".")."VNĐ"."</span>";
     																			echo "<span class='old-price'>". number_format($item->cost,0,",",".")."VNĐ"."</span>";

     																		}
     																	}else{
     																		echo "<a href='' class='new-mark-box'>NEW</a>";
     																	}
																	?>
																	</div>
																</div>
															</div>
														</div>
													</div>
													@endforeach
												</div>
												<!-- HOME2-NEW-PRO-CAROUSEL END -->
											</div>
										</div>
									</div>
								</div>										
							</div>
							<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
								<!-- TOW-COLUMN-ADD START -->
								<div class="tow-column-add zoom-img">
									<a href="#"><img src="img/product/shope-add12.jpg" alt="shope-add" /></a>
								</div>
								<!-- TOW-COLUMN-ADD END -->
							</div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
								<!-- TOW-COLUMN-ADD START -->
								<div class="one-column-add zoom-img">
									<a href="#"><img src="img/product/shope-add22.jpg" alt="shope-add" /></a>
								</div>	
								<!-- TOW-COLUMN-ADD END -->
							</div>	
							<div class="col-xs-12">
								<!-- SALE-PODUCT-AREA START -->
								<div class="sale-poduct-area new-product-area">
									<div class="left-title-area">
										<h2 class="left-title">Sale Products</h2>
									</div>
									<div class="row">
										<!-- HOME2-SALE-CAROUSEL START -->
										<div class="home2-sale-carousel">
											<!-- NEW-PRODUCT SINGLE ITEM START -->
											@foreach($saleproduct as $sale)
											<div class="item">
												<div class="new-product">
													<div class="single-product-item">
														<div class="product-image">
															<a "{{url('ProductDetail',$sale->id)}}"><img src="{{asset ('img/imageProduct/'.$sale->image) }}" alt="product-image" /></a>
															<a href="#" class="new-mark-box">new</a>
															<div class="overlay-content">
																<ul>
																	<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
																	<li><a href="{{url('mua-hang',$sale->id)}}" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
																	<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="product-info">
															<a "{{url('ProductDetail',$sale->id)}}">{{$sale->name}}</a>
															<div class="price-box">
																<span class="price">{{ number_format($sale->cost,0,",",".")}} VNĐ</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
											<!-- NEW-PRODUCT SINGLE ITEM END -->											
										</div>
										<!-- HOME2-SALE-CAROUSEL END -->
									</div>
								</div>
								<!-- SALE-PODUCT-AREA end -->
							</div>
						</div>	
					</div>	
				</div>
			</div>
		</section>	
		<!-- MAIN-CONTENT-SECTION END -->
		<!-- MAIN-CONTENT-SECTION START -->
		<section class="main-content-section-full-column">
			<div class="container">
				<div class="row">
					<!-- IMAGE-ADD-AREA START -->
					<div class="image-add-area">
						<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
							<!-- SINGLE ADD START -->
							<div class="onehalf-add-shope zoom-img">
								<a href="#"><img src="img/product/cms21.jpg" alt="shope-add" /></a>
							</div>
							<!-- SINGLE ADD END -->
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
							<!-- SINGLE ADD START -->
							<div class="onehalf-add-shope zoom-img">
								<a href="#"><img src="img/product/cms22.jpg" alt="shope-add" /></a>
							</div>
							<!-- SINGLE ADD END -->
						</div>						
					</div>
					<!-- IMAGE-ADD-AREA END -->
				</div>
				<div class="row">
					<div class="col-xs-12">
						<!-- FEATURED-PRODUCTS-AREA START -->
						<div class="featured-products-area">
							<div class="left-title-area">
								<h2 class="left-title">Featured Products</h2>
							</div>	
							<div class="row">
								<!-- FEARTURED-CAROUSEL START -->	
								<div class="feartured-carousel">
									<!-- SINGLE ITEM START -->
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/3.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Faded Short Sleeves T-shirt</a>
												<div class="price-box">
													<span class="price">$16.51</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/1.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">sale!</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Blouse</a>
												<div class="price-box">
													<span class="price">$22.95</span>
													<span class="old-price">$27.00</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
									</div>
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/9.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">sale!</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Dress</a>
												<div class="price-box">
													<span class="price">$23.40</span>
													<span class="old-price">$26.00</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/5.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Dress</a>
												<div class="price-box">
													<span class="price">$50.99</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->									
									</div>
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->								
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/12.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$28.98</span>
													<span class="old-price">$30.51</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/13.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$30.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->								
									</div>							
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->							
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/7.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Chiffon Dress</a>
												<div class="price-box">
													<span class="price">$16.40</span>
													<span class="old-price">$20.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/11.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Dress</a>
												<div class="price-box">
													<span class="price">$26.00</span>
												</div>
											</div>
										</div>	
										<!-- SINGLE-PRODUCT-ITEM END -->									
									</div>							
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->					
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/10.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Dress</a>
												<div class="price-box">
													<span class="price">$26.00</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/2.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Blouse</a>
												<div class="price-box">
													<span class="price">$27.00</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->								
									</div>							
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->					
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/4.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Faded Short Sleeves T-shirt</a>
												<div class="price-box">
													<span class="price">$16.51</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/6.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Chiffon Dress</a>
												<div class="price-box">
													<span class="price">$16.40</span>
													<span class="old-price">$20.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->								
									</div>
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->								
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/8.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Dress</a>
												<div class="price-box">
													<span class="price">$26.00</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/13.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$30.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->										
									</div>
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->								
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/12.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$28.98</span>
													<span class="old-price">$30.51</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/3.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$30.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->								
									</div>							
									<!-- SINGLE ITEM END -->
									<!-- SINGLE ITEM START -->								
									<div class="item">
										<!-- SINGLE-PRODUCT-ITEM START -->								
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/7.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-empty"></i>
														<i class="fa fa-star-half-empty"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$28.98</span>
													<span class="old-price">$30.51</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->
										<!-- SINGLE-PRODUCT-ITEM START -->									
										<div class="single-product-item">
											<div class="product-image">
												<a href="#"><img src="img/product/sale/8.jpg" alt="product-image" /></a>
												<a href="#" class="new-mark-box">new</a>
												<div class="overlay-content">
													<ul>
														<li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
														<li><a href="#" title="Quick view"><i class="fa fa-heart-o"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="product-info">
												<div class="customar-comments-box">
													<div class="rating-box">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
													<div class="review-box">
														<span>1 Review(s)</span>
													</div>
												</div>
												<a href="single-product.html">Printed Summer Dress</a>
												<div class="price-box">
													<span class="price">$30.50</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT-ITEM END -->								
									</div>							
									<!-- SINGLE ITEM END -->									
								</div>
								<!-- FEARTURED-CAROUSEL END -->
							</div>
						</div>
						<!-- FEATURED-PRODUCTS-AREA END -->
					</div>						
				</div>
				<div class="row">
					<!-- IMAGE-ADD-AREA START -->
					<div class="image-add-area">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<!-- SINGLE ADD START -->
							<div class="onehalf-add-shope zoom-img">
								<a href="#"><img alt="shope-add" src="img/product/one-helf1.jpg"></a>
							</div>
							<!-- SINGLE ADD END -->
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<!-- SINGLE ADD START -->
							<div class="onehalf-add-shope zoom-img">
								<a href="#"><img alt="shope-add" src="img/product/one-helf2.jpg"></a>
							</div>
							<!-- SINGLE ADD END -->
						</div>						
					</div>
					<!-- IMAGE-ADD-AREA END -->					
				</div>
			</div>
		</section>
		<!-- MAIN-CONTENT-SECTION END -->
		<!-- LATEST-NEWS-AREA START -->
		<section class="latest-news-area">
			<div class="container">
				<div class="row">
					<div class="latest-news-row">
						<div class="center-title-area">
							<h2 class="center-title"><a href="#">latest news</a></h2>
						</div>	
						<div class="col-xs-12">
							<div class="row">
								<!-- LATEST-NEWS-CAROUSEL START -->
								<div class="latest-news-carousel">
									<!-- LATEST-NEWS-SINGLE-POST START -->
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/1.jpg" alt="latest-post" /></a>
												<h2><a href="#">What is Lorem Ipsum?</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/2.jpg" alt="latest-post" /></a>
												<h2><a href="#">Share the Love for printing</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/3.jpg" alt="latest-post" /></a>
												<h2><a href="#">Answers your Questions P..</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/4.jpg" alt="latest-post" /></a>
												<h2><a href="#">What is Bootstrap? – History</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/5.jpg" alt="latest-post" /></a>
												<h2><a href="#">Share the Love for..</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/6.jpg" alt="latest-post" /></a>
												<h2><a href="#">What is Bootstrap? – The History a..</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->	
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/3.jpg" alt="latest-post" /></a>
												<h2><a href="#">Answers your Questions P..</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->
									<!-- LATEST-NEWS-SINGLE-POST START -->								
									<div class="item">
										<div class="latest-news-post">
											<div class="single-latest-post">
												<a href="#"><img src="img/latest-news/4.jpg" alt="latest-post" /></a>
												<h2><a href="#">What is Bootstrap? – History</a></h2>
												<p>Lorem Ipsum is simply dummy text of the printing and Type setting industry. Lorem Ipsum has been...</p>
												<div class="latest-post-info">
													<i class="fa fa-calendar"></i><span>2015-06-20 04:51:43</span>
												</div>
												<div class="read-more">
													<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
									<!-- LATEST-NEWS-SINGLE-POST END -->						
								</div>	
								<!-- LATEST-NEWS-CAROUSEL START -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- LATEST-NEWS-AREA END -->
		<!-- BRAND-CLIENT-AREA START -->
		<section class="brand-client-area">
			<div class="container">
				<div class="row">
					<!-- BRAND-CLIENT-ROW START -->
					<div class="brand-client-row">
						<div class="center-title-area">
							<h2 class="center-title">BRAND & CLIENTS</h2>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<!-- CLIENT-CAROUSEL START -->
								<div class="client-carousel">
									<!-- CLIENT-SINGLE START -->
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/1.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/2.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/3.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/4.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/5.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/1.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->									
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/3.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/2.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/3.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/4.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/5.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/1.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->									
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/3.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/4.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->
									<!-- CLIENT-SINGLE START -->								
									<div class="item">
										<div class="single-client">
											<a href="#">
												<img src="img/brand/5.png" alt="brand-client" />
											</a>
										</div>									
									</div>
									<!-- CLIENT-SINGLE END -->									
								</div>
								<!-- CLIENT-CAROUSEL END -->
							</div>
						</div>
					</div>
					<!-- BRAND-CLIENT-ROW END -->
				</div>
			</div>
		</section>
		<!-- BRAND-CLIENT-AREA END -->
		<!-- COMPANY-FACALITY START -->
		<section class="company-facality">
			<div class="container">
				<div class="row">
					<div class="company-facality-row">
						<!-- SINGLE-FACALITY START -->
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="single-facality">
								<div class="facality-icon">
									<i class="fa fa-rocket"></i>
								</div>
								<div class="facality-text">
									<h3 class="facality-heading-text">FREE SHIPPING</h3>
									<span>on order over $100</span>
								</div>
							</div>
						</div>
						<!-- SINGLE-FACALITY END -->
						<!-- SINGLE-FACALITY START -->
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="single-facality">
								<div class="facality-icon">
									<i class="fa fa-umbrella"></i>
								</div>
								<div class="facality-text">
									<h3 class="facality-heading-text">24/7 SUPPORT</h3>
									<span>online consultations</span>
								</div>
							</div>
						</div>
						<!-- SINGLE-FACALITY END -->
						<!-- SINGLE-FACALITY START -->						
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="single-facality">
								<div class="facality-icon">
									<i class="fa fa-calendar"></i>
								</div>
								<div class="facality-text">
									<h3 class="facality-heading-text">DAILY UPDATES</h3>
									<span>Check out store for latest</span>
								</div>
							</div>
						</div>
						<!-- SINGLE-FACALITY END -->
						<!-- SINGLE-FACALITY START -->						
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="single-facality">
								<div class="facality-icon">
									<i class="fa fa-refresh"></i>
								</div>
								<div class="facality-text">
									<h3 class="facality-heading-text">30-DAY RETURNS</h3>
									<span>moneyback guarantee</span>
								</div>
							</div>
						</div>		
						<!-- SINGLE-FACALITY END -->					
					</div>
				</div>
			</div>
		</section>
		<!-- COMPANY-FACALITY END -->
		<!-- FOOTER-TOP-AREA START -->
		<section class="footer-top-area">
			<div class="container">
				<div class="footer-top-container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<!-- FOOTER-TOP-LEFT START -->
							<div class="footer-top-left">
								<!-- NEWSLETTER-AREA START -->
								<div class="newsletter-area">
									<h2>Newsletter</h2>
									<p>Subscribe to our mailing list to receive updates on new arrivals, special offers and other discount information.</p>
									<form action="#">
										<div class="form-group newsletter-form-group">
										  <input type="text" class="form-control newsletter-form" placeholder="Enter your e-mail">
										  <input type="submit" class="newsletter-btn" name="submit" value="Subscribe" />
										</div>
									</form>
								</div>
								<!-- NEWSLETTER-AREA END -->
								<!-- ABOUT-US-AREA START -->
								<div class="about-us-area">
									<h2>About Us</h2>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
								</div>
								<!-- ABOUT-US-AREA END -->
								<!-- FLLOW-US-AREA START -->
								<div class="fllow-us-area">
									<h2>Follow us</h2>
									<ul class="flow-us-link">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-rss"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
								<!-- FLLOW-US-AREA END -->
							</div>
							<!-- FOOTER-TOP-LEFT END -->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<!-- FOOTER-TOP-RIGHT-1 START -->
							<div class="footer-top-right-1">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hidden-sm">
										<!-- STATICBLOCK START -->
										<div class="staticblock">
											<h2>static block</h2>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s<br />when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
										</div>
										<!-- STATICBLOCK END -->
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<!-- STORE-INFORMATION START -->
										<div class="Store-Information">
											<h2>Store Information</h2>
											<ul>
												<li>
													<div class="info-lefticon">
														<i class="fa fa-map-marker"></i>
													</div>
													<div class="info-text">
														<p>My Company, 42 avenue des Champs Elysées 75000 Paris France </p>
													</div>
												</li>
												<li>
													<div class="info-lefticon">
														<i class="fa fa-phone"></i>
													</div>
													<div class="info-text call-lh">
														<p>Call us now : 0123-456-789</p>
													</div>
												</li>
												<li>
													<div class="info-lefticon">
														<i class="fa fa-envelope-o"></i>
													</div>
													<div class="info-text">
														<p>Email : <a href="mailto:sales@yourcompany.com"><i class="fa fa-angle-double-right"></i> sales@yourcompany.com</a></p>
													</div>
												</li>
											</ul>
										</div>
										<!-- STORE-INFORMATION END -->
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<!-- GOOGLE-MAP-AREA START -->
										<div class="google-map-area">
											<div class="google-map">
												<div id="googleMap" style="width:100%;height:150px;">
													
												</div>
											</div>
										</div>
										<!-- GOOGLE-MAP-AREA END -->
									</div>
								</div>
							</div>
							<!-- FOOTER-TOP-RIGHT-1 END -->
							<div class="footer-top-right-2">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<!-- FOTTER-MENU-WIDGET START -->
										<div class="fotter-menu-widget">
											<div class="single-f-widget">
												<h2>Categories</h2>
												<ul>
													<li><a href="shop-gird.html"><i class="fa fa-angle-double-right"></i>Women </a></li>
													<li><a href="shop-gird.html"><i class="fa fa-angle-double-right"></i>Men</a></li>
													<li><a href="shop-gird.html"><i class="fa fa-angle-double-right"></i>clothing</a></li>
													<li><a href="shop-gird.html"><i class="fa fa-angle-double-right"></i>kids</a></li>
												</ul>
											</div>
										</div>
										<!-- FOTTER-MENU-WIDGET END -->
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<!-- FOTTER-MENU-WIDGET START -->
										<div class="fotter-menu-widget">
											<div class="single-f-widget">
												<h2>Information</h2>
												<ul>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>Specials</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>New products</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>Best sellers</a></li>
													<li><a href="contact-us.html"><i class="fa fa-angle-double-right"></i>Contact Us</a></li>
												</ul>
											</div>
										</div>
										<!-- FOTTER-MENU-WIDGET END -->
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<!-- FOTTER-MENU-WIDGET START -->
										<div class="fotter-menu-widget">
											<div class="single-f-widget">
												<h2>My account</h2>
												<ul>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>My orders</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>My credit slips</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>My addresses</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>My personal info</a></li>
													<li><a href="#"><i class="fa fa-angle-double-right"></i>Sign out</a></li>
												</ul>
											</div>
										</div>
										<!-- FOTTER-MENU-WIDGET END -->
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<!-- PAYMENT-METHOD START -->
										<div class="payment-method">
											<img class="img-responsive pull-right" src="img/payment.png" alt="payment-method" />
										</div>
										<!-- PAYMENT-METHOD END -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- FOOTER-TOP-AREA END -->
		<!-- COPYRIGHT-AREA START -->
		<footer class="copyright-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="copy-right">
							<address>Copyright © 2015 <a href="http://bootexperts.com/">BootExperts</a> All Rights Reserved</address>
						</div>
						<div class="scroll-to-top">
							<a href="#" class="bstore-scrollertop"><i class="fa fa-angle-double-up"></i></a>
						</div>
					</div>
				</div>
			</div>
		</footer> 
		<!-- COPYRIGHT-AREA END -->
		<!-- JS 
		===============================================-->
		<!-- jquery js -->
		<script src="{{ asset('js/vendor/jquery-1.11.3.min.js') }}"></script>
		
		<!-- fancybox js -->
        <script src="{{ asset('js/jquery.fancybox.js') }}"></script>
		
		<!-- bxslider js -->
        <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
		
		<!-- meanmenu js -->
        <script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
		
		<!-- owl carousel js -->
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
		
		<!-- nivo slider js -->
        <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
		
		<!-- jqueryui js -->
        <script src="{{ asset('js/jqueryui.js') }}"></script>
		
		<!-- bootstrap js -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
		
		<!-- wow js -->
        <script src="{{ asset('js/wow.js') }}"></script>		
		<script>
			new WOW().init();
		</script>

		<!-- Google Map js -->
        <script src="https://maps.googleapis.com/maps/api/js"></script>	
		<script>
			function initialize() {
			  var mapOptions = {
				zoom: 8,
				scrollwheel: false,
				center: new google.maps.LatLng(35.149868, -90.046678)
			  };
			  var map = new google.maps.Map(document.getElementById('googleMap'),
				  mapOptions);
			  var marker = new google.maps.Marker({
				position: map.getCenter(),
				map: map
			  });

			}
			google.maps.event.addDomListener(window, 'load', initialize);				
		</script>
		<!-- main js -->
        <script src="{{ asset('js/main.js') }}"></script>
    </body>

<!-- Nulled by http://www.baobinh.net by tieulonglanh -->
</html>