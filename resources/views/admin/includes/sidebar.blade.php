
		<div class="page-sidebar navbar-collapse collapse">

			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper">

					<div class="sidebar-toggler">
					</div>
				</li>
				<li class="sidebar-search-wrapper">
					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="SearchByName...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
				</li>
				<li>
					<a href="{{url('/home')}}">
					<i class="icon-home"></i>
					<span class="title">Home</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/category/show')}}">
					<i class="icon-home"></i>
					<span class="title">Category</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li class="active open">
					<a href="{{url('/admin/user/show')}}">
					<i class="icon-user"></i>
					<span class="title">User Managerment</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/product/show')}}">
					<i class="icon-product"></i>
					<span class="title">Product Managerment</span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/sale/show')}}">
					<i class="icon-product"></i>
					<span class="title">Sale Managerment</span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href= "{{url('auth/logout')}}" >
					<i class="icon-settings"></i>
					Log out</a>
				</li>				
			</ul>
		</div>