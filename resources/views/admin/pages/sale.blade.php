@extends('admin.layouts.default')
@section('content')
	<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
					<!-- Begin: life time stats -->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Create new sale
						</div>	
	<div class="portlet box purple ">
		<div class="portlet-body form col-sm-7">
			@include('errors.validate')
			<form method="POST" action="{{url('postsetsale/{product->id}')}}" enctype= "multipart/form-data">
				{!! csrf_field() !!}
				<div class="form-group">
					Product Name
					<input type="text"  disabled = "disabled" class = "form-control" name="product" value="{{ $product->name }}">
					<input type ="hidden" value="{{ $product->id }}" class = "form-control" name = "product_id">
				</div> 							
				<div class="form-group">
					Percent Sale
					<input type="number" class = "form-control" name="percent" >
				</div>
				<div class="form-group">
					From Date
					<input type = "text" placeholder = "yyyy-mm-dd" class = "form-control" name = "fromdate">
				</div>
				<div class="form-group">
					To Date
					<input type = "text" placeholder="yyyy-mm-dd" class = "form-control" name = "todate">
				</div>
				<div class="form-actions right1 ">
					<a href="{{url('/admin/product/show')}}" type="button" id="back-btn" class="btn btn-default">Back</a>
					<button type="submit" class="btn green"> Add </button>
				</div>
			</form>
		</div>
	</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop