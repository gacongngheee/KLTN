@extends('admin.layouts.default')
@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			User<small>>list</small>
			</h3>
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>User
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
			
								<table class="table table-striped table-bordered table-hover" id="datatable_products">
								<thead>
								<tr role="row" class="heading">
									<th width="1%">
										<input type="checkbox" class="group-checkable">
									</th>
									<th width="10%">
										 ID
									</th>
									<th width="15%">
										 Name
									</th>
									<th width="15%">
										 email
									</th>
									<th width="10%">
										 Avatar
									</th>
									<th width="15%">
										 Created-at
									</th>
									<th width="15%">
										 Update-at
									</th>
									<th width = "15%" th colspan="3" style = "text-align: center">
										Action
									</th>
								</tr>
								</thead>
								<tbody>
									@foreach ($users as $user)
									<tr>
										<td width="1%"><input type="checkbox" class="group-checkable"></td>
										<td>{{$user->id}}</td>
										<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
										<td>
      										<img src="../../img/{{ $user->avatar }}" alt="avatar" width="50" heigh="50">
     									</td>
										<td>{{$user->created_at}}</td>
										<td>{{$user->updated_at}}</td>
             							<td><a href="{{url('admin/user/edit',$user->id)}}" class="btn btn-warning">Update</a></td>
             							<td><a onclick = "return check()" class = "btn btn-danger" href ="{{URL::to('admin/user/delete/'.$user->id) }}">Delete</a></td>		
									</tr>
									@endforeach
								</tbody>
								</table>
							</div>
						</div>
						<div class  = "row">
                    		<ul class = "pagination pull-right">
                      			@if($users->currentPage() != 1)
                      			<li><a href = "{{ str_replace('/?','?',$users->url($users->currentPage() -1)) }}">Prev</a>
                        		@endif
                        		@for($i = 1; $i<= $users->lastPage(); $i = $i+1)
                        		<li class = "{{ ($users->currentPage() == $i) ? 'active' : '' }}">
                          		<a href = "{{ str_replace('/?','?',$users->url($i)) }}"> {{ $i }}</a>
                        		</li>
                        		@endfor
                       		 	@if ($users->currentPage() != $users->lastPage())
                        		<li><a href = "{{ str_replace('/?','?',$users->url($users->currentPage() +1)) }}">Next</a>
                          		@endif
                        		</li>
                    		</ul>
              			</div>
					</div>
				</div>
			</div>
		</div>
@stop