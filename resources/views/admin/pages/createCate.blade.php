@extends('admin.layouts.default')
@section('content')
	<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
					<!-- Begin: life time stats -->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Create new Categories
						</div>	
	<div class="portlet box purple ">
		<div class="portlet-body form col-sm-7">
			@include('errors.validate')
			<form method="POST" action="{{url('admin/category/post/{category->id}')}}" enctype= "multipart/form-data">
				{!! csrf_field() !!}
				<div class="form-group">
					ParentID
					<input type="text"  disabled = "disabled" class = "form-control" name="parentId" value="{{ $category->id }}">
					<input type ="hidden" value="{{ $category->id }}" class = "form-control" name = "parentId">
				</div> 							
				<div class="form-group">
					Name
					<input type="text" class = "form-control" name="name" >
				</div>
				<div class="form-actions right1 ">
					<a href="{{url('/admin/category/show')}}" type="button" id="back-btn" class="btn btn-default">Back</a>
					<button type="submit" class="btn green"> Add </button>
				</div>
			</form>
		</div>
	</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop