@extends('admin.layouts.homeTemp')
@section('content')
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>

				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->

					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li>
					<a href="{{url('/home')}}">
					<i class="icon-home"></i>
					<span class="title">Home</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/category/show')}}">
					<i class="fa fa-gift"></i>
					<span class="title">Category</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('admin/user/show')}}">
					<i class="icon-user"></i>
					<span class="title">User Managerment</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li class="active open">
					<a href="javascript:;">
					<i class="icon-product"></i>
					<span class="title">Product Managerment</span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{url('/admin/product/show')}}"s>
							<i class="icon-wallet"></i>
							List</a>
							<span class = "seclected"> </span>
							<span class="arrow open"></span>
						</li>
						<li  class="active open">
							<a href="{{url('/admin/product/create')}}">
							<i class="icon-settings"></i>
							Create</a>
						</li>
					</ul>
				</li>
				<li>
					<a href= "{{url('auth/logout')}}" >
					<i class="icon-settings"></i>
					Log out</a>
				</li>				
			</ul>
		</div>
<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			Product <small>>create Product</small>
			</h3>	
			<div class="portlet-body">
								<div class="tabbable">
									<ul class="nav nav-tabs">
										<li class="active">
											<a href="#tab_general" data-toggle="tab">
											General </a>
										</li>
										<li>
											<a href="#tab_images" data-toggle="tab">
											Images </a>
										</li>
									</ul>
			<div class="tab-content no-space">
				<div class="tab-pane active" id="tab_general">
					<div class="form-body">
					@include('errors.validate')
				<form method="POST" action="{{url('/admin/product/store')}}" enctype= "multipart/form-data">
    				{!! csrf_field() !!}
    				<div class="form-group">
       				 	Name
        				<input type="text" class = "form-control" name="name">
    				</div> 

    				<div class="form-group">
						category
						<select name = "categoryId" class="form-control">
							<option value = "0">All Categories</option>
							<?php App\Libraries\MyFunction::cate_parent($category); ?>
						</select>
					</div>
    				<div class="form-group">
        				description
        				<textarea class = "form-control" name="description"> </textarea>
    				</div >

    				<div class="form-group">
        				cost
        				<input type="text" class = "form-control" name="cost">
    				</div>
    				<div class="form-group">
        				Quantity
        				<input type="text" class = "form-control" name="quantity">
    				</div>
    				<div class="form-group">
        				Image
        				<input type="file" class = "form-control" name="image">
    				</div>

    				<div class="form-actions">
        				<button type="submit" class="btn btn-success uppercase pull-left">Submit</button>
    				</div>
				</div>
			</div>

			<div class="tab-pane" id="tab_images">
			<div>
				@for($i = 1; $i <=5; $i++)
				<div class = "form-group">
					<label> Image Product Detail {{ $i }} </label>
					<input type = "file" name = "productDetail[]" />
				</div>
				@endfor	
			</div>
			</form> 										
			</div>

				</div>
			</div>
		</div>
	</div>
</div>


@stop