@extends('admin.layouts.homeTemp')
@section('content')
<style>
	.img_current{width: 150px;height: 150px; margin}
	.img_detail{width: 300px;height: 300px; margin-bottom: 20px;}
	.icon_del {position: relative; top:-150px; left:-30px;}

</style>
	<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>

				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->

					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li>
					<a href="{{url('/home')}}">
					<i class="icon-home"></i>
					<span class="title">Home</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/category/show')}}">
					<i class="fa fa-gift"></i>
					<span class="title">Category</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('admin/user/show')}}">
					<i class="icon-user"></i>
					<span class="title">User Managerment</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li class="active open">
					<a href="javascript:;">
					<i class="icon-product"></i>
					<span class="title">Product Managerment</span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{url('/admin/product/show')}}"s>
							<i class="icon-wallet"></i>
							List</a>
							<span class = "seclected"> </span>
							<span class="arrow open"></span>
						</li>
						<li>
							<a href="{{url('/admin/product/create')}}">
							<i class="icon-settings"></i>
							Create</a>
						</li>
					</ul>
				</li>
				<li>
					<a href= "{{url('auth/logout')}}" >
					<i class="icon-settings"></i>
					Log out</a>
				</li>				
			</ul>
		</div>
	<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
					<!-- Begin: life time stats -->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Edit Product
						</div>
					</div>
<div class="portlet-body">
		@include('errors.validate')
		<form method="POST" action="{{url('admin/product/update/{product->id}')}}" name="frmEditProduct" enctype= "multipart/form-data">
				{!! csrf_field() !!}
		<div class="tabbable">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab_general" data-toggle="tab">
				General </a>
			</li>
			<li>
				<a href="#tab_images" data-toggle="tab">
				Images </a>
			</li>
		</ul>
		</div>
			<div class="portlet-body form col-sm-7">
				<div class="tab-content no-space">
					<div class="tab-pane active" id="tab_general">
						<div class="form-body">
					<div class="form-group">
       				 	Name
        				<input type="text" class = "form-control" name="name" value = "{{$product->name}}">
    					<input type="hidden" class = "form-control" name="id" value="{{ $product->id }}">
    				</div> 
    				<div class="form-group">
						category
						<select name = "categoryId" class="form-control">
							<?php App\Libraries\MyFunction::cate_parent($category); ?>
						</select>
					</div>
    				<div class="form-group">
        				description
        				<input class = "form-control" name="description" value = "{{$product->description}}"> 
    				</div >
    				<div class="form-group">
        				Quantity
        				<input class = "form-control" name="quantity" value = "{{$product->quantity}}"> 
    				</div >

    				<div class="form-group">
        				cost
        				<input type="text" class = "form-control" name="cost" value = "{{$product->cost}}">
    				</div>
    				<div class="form-group">
        				Image Current
        				<img src ="{{asset('/img/imageProduct/'.$product->image)}}" class = "img_current"/>
        				<input type = "hidden" name = "img_current" value = "{{$product['image'] }}" />
        				
    				</div>
    				<div class="form-group">
        				Image
        				<input type="file" class = "form-control" name="image">
    				</div>
					<div class="form-actions right1 ">
						<a href="{{url('/admin/product/show')}}" type="button" id="back-btn" class="btn btn-default">Back</a>
						<button type="submit" class="btn green"> Edit </button>
					</div>
			</div>
		</div>
		<div class="tab-pane" id="tab_images">
			@foreach($product_image as $key => $item)
			<div class = "form-group" id = "{{ $key }}">
			<img src ="{{asset('/img/imageProduct/imgDetail/'.$item->image)}}" class = "img_detail" idHinh="{{ $item->id }}" id = "{{ $key }}"/>
			<a href = "javascript:" type = "button" id = "del_img_demo" class = "btn btn-danger btn-circle icon_del"><i class = "fa fa-times"></i></a>
		</div>
			@endforeach
		<button type = "button" class = "btn btn-primary" id = "addImages"> Add Images</button>
		<div id = "insert"> </div>
		</div>
					</div>
				</div>
			</div>
	
	</div>
</div>
</div>
</div>
</div>

@stop