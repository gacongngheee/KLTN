@extends('admin.layouts.default')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">

	   <h3 class="page-title">
			User <small>>EDIT USER</small>
		</h3>	
		<div class="row">
			<div class="col-md-6">
                <h1>EDIT</h1>
                @include('errors.validate')
                <form method="POST" action="{{url('admin/user/update')}}" enctype= "multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        Name
                        <input type="text" class = "form-control" name="name" value="{{ $user->name }}">
                        <input type="hidden" class = "form-control" name="id" value="{{ $user->id }}">
                    </div> 
                    <div class="form-group">
                        Email
                        <input type="email" class = "form-control" name="email" value="{{ $user->email }}">
                    </div >
                    <div class="form-group">
                        Avatar
                        <input type="file" class = "form-control" name="avatar">
                    </div>
                    <div class="form-group">
                        <button type="submit" class = 'btn btn-primary form-control'>EDIT</button>
                    </div>
                </form>											
				</div>
			</div>
		</div>
	</div>
@stop