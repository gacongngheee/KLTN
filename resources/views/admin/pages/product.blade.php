@extends('admin.layouts.homeTemp')
@section('content')

		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>

				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->

					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li>
					<a href="{{url('/home')}}">
					<i class="icon-home"></i>
					<span class="title">Home</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('/admin/category/show')}}">
					<i class="fa fa-gift"></i>
					<span class="title">Category</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li>
					<a href="{{url('admin/user/show')}}">
					<i class="icon-user"></i>
					<span class="title">User Managerment</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
				</li>
				<li class="active open">
					<a href="javascript:;">
					<i class="icon-product"></i>
					<span class="title">Product Managerment</span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<li class="active open">
							<a href="{{url('/admin/product/show')}}"s>
							<i class="icon-wallet"></i>
							List</a>
							<span class = "seclected"> </span>
							<span class="arrow open"></span>
						</li>
						<li>
							<a href="{{url('/admin/product/create')}}">
							<i class="icon-settings"></i>
							Create</a>
						</li>
					</ul>
				</li>
				<li>
					<a href= "{{url('auth/logout')}}" >
					<i class="icon-settings"></i>
					Log out</a>
				</li>				
			</ul>
		</div>
	<div class="page-content-wrapper">
		<div class="page-content">
			Product<small>>list</small>
			</h3>
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<table class="table table-striped table-bordered table-hover" id="datatable_products">
								<thead>
								<tr role="row" class="heading">
									<th width="1%">
										<input type="checkbox" class="group-checkable">
									</th>
									<th width="5%">
										 ID
									</th>
									<th width="12%">
										 Name
									</th>
									<th width="5%">
										 CategoryId
									</th>
									<th width="13%">
										 cost
									</th>
									<th width="10%">
										 Quantity
									</th>
									<th width="15%">
										 image
									</th>
									<th width=" = 13%">
										Sale
									</th>
									<th width = "15%" th colspan="3" style = "text-align: center">
										Action
									</th>
								</tr>
								</thead>
								<tbody>
									@foreach ($products as $product)
									<tr>
										<td width="1%"><input type="checkbox" class="group-checkable"></td>
										<td>{{$product->id}}</td>
										<td>{{$product->name}}</td>
										<td>{{$product->categoryId}}</td>
										<td>{{ number_format($product->cost,0,",",".") }} VNĐ</td>
										<td>{{$product->quantity}}</td>
										<td>
										@if ($product->image!='')
      										<img src="../../img/imageProduct/{!!$product->image!!}" alt="avatar" width="50" heigh="50">
     									@else
      										<img src="img/default.png" alt="avatar" width="50" heigh="50">
     									@endif
     									</td>
     									<td>
     									<?php 
     									if($product->sale == 1){
     										$sale1 = DB::table('sale')->select('todate')->where('product_id',$product->id)->first();
     										$datenow=date("Y-m-d");
     										if($sale1->todate < $datenow ){
     											echo "NO";
     										}else{
     											echo "SALE";
     										}
     									}else{
     										echo "NO";
     									}

     									?>
     									</td>

										<td><a href="{{url('setsale',$product->id)}}" class="btn btn-primary">SetSale</a></td>
             							<td><a href="{{url('admin/product/edit',$product->id)}}" class="btn btn-warning">Edit</a></td>
             							<td><a onclick = "return check()" class = "btn btn-danger" href ="{{URL::to('admin/product/delete/'.$product->id) }}">Delete</a></td>
             							</td>			
									</tr>
									@endforeach
								</tbody>
								</table>
							</div>
						</div>
						<div class  = "row">
                    		<ul class = "pagination pull-right">
                      			@if($products->currentPage() != 1)
                      			<li><a href = "{{ str_replace('/?','?',$products->url($products->currentPage() -1)) }}">Prev</a>
                        		@endif
                        		@for($i = 1; $i<= $products->lastPage(); $i = $i+1)
                        		<li class = "{{ ($products->currentPage() == $i) ? 'active' : '' }}">
                          		<a href = "{{ str_replace('/?','?',$products->url($i)) }}"> {{ $i }}</a>
                        		</li>
                        		@endfor
                       		 	@if ($products->currentPage() != $products->lastPage())
                        		<li><a href = "{{ str_replace('/?','?',$products->url($products->currentPage() +1)) }}">Next</a>
                          		@endif
                        		</li>
                    		</ul>
              			</div>
					</div>
				</div>
			</div>

@stop