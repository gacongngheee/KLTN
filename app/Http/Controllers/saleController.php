<?php

namespace App\Http\Controllers;

use DB;
use App\product;
use App\sale;
use App\productImage;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaleRequest;
use Input;
use File;
use Intervention\Image\Facades\Image;

class saleController extends Controller
{
    public function get($id){        
        $product = Product::find($id);
        return view('admin.pages.sale')->with('product', $product);
    }
    public function set(SaleRequest $request){
        $input = $request->all();
        $id = $request->input('product_id');

        $sale1 = DB::table('sale')->where('product_id',$id);
        if(!empty($sale1)){
            $sale1->delete();
        }
        $sale = new sale;
        $sale->product_id = $id;
        $sale->percent = $request->input('percent');
        $sale->fromdate = $request->input('fromdate');
        $sale->todate = $request->input('todate');
        $sale->save();
        $product = product::find($id);
        $product->sale = 1;
        $product->save();
        return redirect('admin/product/show');
    }
}