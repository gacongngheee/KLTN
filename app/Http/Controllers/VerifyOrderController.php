<?php 
namespace App\Http\Controllers;

use DB;
use App\product;
use App\customer;
use App\order;
use App\orderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use Input,Mail;
use File;
use Cart;


// Lop xac thuc tai khoan nguoi dung

class VerifyOrderController extends Controller {

    public function confirm($key_active)
    {
        $order = Order::wherekey_active($key_active)->first();
        $order->actived = 1;
        $order->key_active = null;
        $order->save();
        /*$orderDetail = DB::table('orderDetail')->select('product_id','quantity')->where('order_id',$orderId);
        foreach($orderDetail as $key){
            $product = Product::find($key->product_id);
            $product->quantity -= $key->quantity;
            $product->save();
        } */
        return Redirect('home');
    }
}