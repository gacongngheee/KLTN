<?php

namespace App\Http\Controllers;

use DB;
use App\product;
use App\categories;
use App\productImage;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Input;
use File;
use Intervention\Image\Facades\Image;

class productController extends Controller
{
    public function showProduct(){        
        $products =  DB::table('product')->paginate(10);
        $category = categories::all();
        return view('admin.pages.product')->with('products', $products)
                                    ->with('category', $category);
    }
    public function getCreate(){
        $category = categories::select('id','name','parentId')->get()->toArray();
        return view('admin.pages.createProduct',compact('category'));
    }
    public function store(ProductRequest $request){

        $input = $request->all();
        
        $product = new product;
        
        $product->name = $input["name"];
        
        $product->categoryId = $input["categoryId"];
        $product->description = $input["description"];
        $product->cost = $input["cost"];
        $product->quantity = $input["quantity"];
        $imageP = Input::file('image');
        $image_temp = $product->name.'.'.rand(0,9999);
        $product->image = productController::to_slug($image_temp).'.'. $request->file('image')->getClientOriginalExtension();
        $path = public_path('/img/imageProduct/'.$product->image);
        Image::make($imageP->getRealPath())->resize(540, 720)->save($path);
        $product->save();
        $product_id = $product->id;
        if(Input::hasFile('productDetail')) {
            foreach (Input::file('productDetail') as $file) {
            $product_img = new productImage();
            if(isset($file)) {
                $product_imgTemp = $product->name.'.' .rand(0,9999);
                $product_img->image = productController::to_slug($product_imgTemp).'.'. $file->getClientOriginalExtension();
                $pathD = public_path('/img/imageProduct/imgDetail/'.$product_img->image);
                Image::make($file->getRealPath())->resize(540, 720)->save($pathD);
                $product_img->product_Id = $product_id;
                $product_img->save();
            }
        }
        }
        return redirect('admin/product/show');
    }
    public function editProduct($id)
    {
        $product = Product::find($id);
        $category = categories::select('id','name','parentId')->get()->toArray();
        $product_image = Product::find($id)->pImage;
        return view('admin.pages.editProduct',compact('category'))->with('product', $product)
                                        ->with('product_image', $product_image);
    }
    public function updateProduct(ProductRequest $request)
    {
        $input = $request->all();
        $id = $request->input('id');
        $product = Product::find($id);
        $product->name = $request->input('name',50);
        $product->categoryId = $request->input('categoryId',50);
        $product->description = $request->input('description');
        $product->quantity = $request->input('quantity');
        $product->cost = $request->input('cost');
        $imageN = $request->file('image');
        $img_current = 'public/img/imageProduct/'.$request->input('img_current');
        if(!empty($imageN)){
        $imageTemp = $product->name.'.'.rand(0,9999);
        $product->image = productController::to_slug($imageTemp).'.'. $request->file('image')->getClientOriginalExtension();
        $path = public_path('/img/imageProduct/'.$product->image);
        Image::make($imageN->getRealPath())->resize(540, 720)->save($path);
        if(File::exists($img_current)) {
            File::delete($img_current);
            }
        } 
        $product->save();
        if (!empty($request->file('fEditDetail'))) {
            foreach ($request->file('fEditDetail') as $file) {
                $product_img = new ProductImage();
                if(isset($file)) {
                    $temp = $product->name.'.'.rand(0,9999);
                    $product_img->image = productController::to_slug($temp).'.'. $file->getClientOriginalExtension();
                    $product_img->product_Id = $id;
                    $pathD = public_path('/img/imageProduct/imgDetail/'.$product_img->image);
                    Image::make($file->getRealPath())->resize(540, 720)->save($pathD);
                    $product_img->save();
                }
            }
        }
        return redirect('admin/product/show');
    }
    public function getDelImg ($id, ProductRequest $request) {

        if ($request->ajax()) {
            $idHinh = (int)$request->get('idHinh');
            $image_detail = ProductImage::find($idHinh);
            if(!empty($image_detail)) {
                $img = 'img/imageProduct/imgDetail/'.$image_detail->image;
                if(File::exists($img)) {
                    File::delete($img);
                }
                $image_detail->delete();
            }
            return "Oke";
        }
    }
    public function destroyProduct ($id) {
        $product_detail = Product::find($id)->pimage->toArray();
        foreach ($product_detail as $value) {
            $image = productImage::find($value['id']);
            $image->delete();
            File::delete('img/imageProduct/imgDetail/'.$value["image"]);
        }
        $product = Product::find($id);
        File::delete('img/imageProduct/'.$product->image);
        $product->delete($id);
        return redirect('admin/product/show');
    }
    public function to_slug($str) {
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    $str = preg_replace('/([\s]+)/', '-', $str);
    return $str;
    }
}