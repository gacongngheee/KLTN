<?php 
namespace App\Http\Controllers;

use DB;
use App\product;
use App\categories;
use App\productImage;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use File;
use Cart;

class homeController extends Controller {

	public function index()
	{	
		$datenow=date("Y-m-d");
		$newProducts = DB::table('product')->select('id','name','image','quantity','cost','categoryId','sale')
											->orderby('id', 'desc')
											->skip(0)->take(10)
											->get();
		$bestproduct = Product::select(array('id','name','image','quantity','cost','categoryId', 'sold', 'sale'))
											->whereBetween(DB::raw("DATEDIFF(updated_at,created_at)"),[0,30])
											->orderby('sold','desc')
											->skip(0)->take(10)
											->get();
		/*$saleproduct = DB::table('product')->join('sale','id','=','sale.product_id')
											->select('id','name','image','quantity','cost','categoryId', 'sale')
											->where('sale',1)
											->whereBetween(DB::raw("DATEDIFF('date')"))
											->orderby('id','desc')
											->skip(0)->take(10)
											->get();*/
		$category = categories::all();
		$content = Cart::content();
		$total = Cart::total();
		$count = Cart::count();
		return view('frontend/home',compact('newProducts','category', 'category','saleproduct', 'content','bestproduct', 'total','count'));
	}
	public function loaisanpham ($id) {
		$product_cate = DB::table('product')->select('id','name','image','cost','quantity','categoryId')->where('categoryId',$id)->paginate(1);
		$category = categories::all();
		$content = Cart::content();
		$total = Cart::total();
		$count = Cart::count();
		return view('frontend/byCate',compact('product_cate','content','category', 'total','count'));

	}
	public function ProductDetail($id){
		$product_detail = DB::table('product')->where('id',$id)->first();
		$image = DB::table('productImage')->select('id','image')->where('product_id',$product_detail->id)->get();
		$category = categories::all();
		$content = Cart::content();
		$total = Cart::total();
		$count = Cart::count();
		return view('frontend.productDetail',compact('product_detail','image','category', 'content','total','count'));
	}
	public function muahang ($id){
		$product_buy = DB::table('product')->where('id',$id)->first();
		Cart::add(array('id'=>$id,'name'=>$product_buy->name,'qty'=>1,'price'=>$product_buy->cost,'options' =>array('img'=> $product_buy->image)));
		$content = Cart::content();
		return Redirect::back();
	}
	public function giohang () {
		$content = Cart::content();
		$category = categories::all();
		$total = Cart::total();
		$count = Cart::count();
		return view('frontend.cart',compact('content','total','category', 'count'));
	}
	public function xoagiohang ($id) {
		Cart::remove($id);
		return Redirect::back();
	}
	public function searchbycategory(){
		$search = \Request::get('searchcate');
		$category_id = \Request::get('cateId');
		$category = categories::all();
		$content = Cart::content();
		$total = Cart::total();
		$count = Cart::count();
		if($category_id == 0){
			$productResult = DB::table('product')->select('id','name','image','quantity','cost','categoryId')->orderby('id','desc')->paginate(1);
		}else{
			$productResult = DB::table('product')->select('id','name','image','cost','quantity','categoryId')
												->where('categoryId',$category_id)
												->where('name','like','%'.$search.'%')->paginate(5);
		}
		return view('frontend.SearchResults', compact('category','content','total','count'))->with('productResult',$productResult);
	}
	public function test(){
		$bestproduct = Product::select(array('id','name','image','quantity','cost','categoryId', 'sold'))
								->whereBetween(DB::raw("DATEDIFF(updated_at,created_at)"),[0,30])
								->orderby('sold','desc')->get();
		return view('frontend.test')->with('bestproduct',$bestproduct);
	}
}