<?php 
namespace App\Http\Controllers;

use DB;
use App\product;
use App\customer;
use App\order;
use App\orderDetail;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use Input,Mail;
use File;
use Cart;

class orderController extends Controller {
	public function getcheckout () {
                $content = Cart::content();
                $total = Cart::total();
                $count = Cart::count();
		return view('frontend.checkout',compact('content','total','count'));
	}
	public function postcheckout (OrderRequest $request) {
        $input = $request->all();
        $content = Cart::content();
        $total = Cart::total();
        
        $customer = new customer;
        
        $customer->firstname = $input["firstname"];
        $customer->lastname = $input["lastname"];
        $customer->email = $input["emailCustomer"];
        $customer->phone = $input["phoneCustomer"];
        $customer->address = $input["addressCustomer"];
        $customer->address2 = $input["address2Customer"];
        $customer->city = $input["cityCustomer"];
        $customer->save();

        $order = new order;
        $order->customer_id = $customer->id;
        $order->key_active = rand(1,1000);
        $order->actived = 0;
        Mail::send('frontend.verifyOrder',['key_active'=>$order->key_active,'orderId'=>$order->id, 'content'=> $content,'total'=> $total] , function ($message) use ($input)
            {   
            $message->from('trananh84488@gmail.com', 'admin');
            $message->to($input['emailCustomer'], $input['lastname'])->subject('Xác nhận đơn hàng');
            });  
        $order->save();

        foreach ($content as $key) {
        $orderDetail = new orderDetail;
        $orderDetail->order_id = $order->id;
        $orderDetail->product_id = $key->id;
        $orderDetail->quantity = $key->qty;
        $orderDetail->save();
        $product = Product::find($key->id);
        $product->quantity -= $key->qty;
        $product->sold += $key->qty;
        
        $product->save();
        }
        Cart::destroy();
        return redirect('home');
	}
}