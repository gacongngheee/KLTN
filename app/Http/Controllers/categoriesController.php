<?php

namespace App\Http\Controllers;

use DB;
use App\categories;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CateRequest;

class categoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getCate(){
       
        
        $categories =  DB::table('category')->paginate(50);
        $category=CategoriesController::dequy($categories,0,0);
        return view('admin.pages.category')->with('category', $category)
                                           ->with('categories',$categories);
    }
    public function create($id)
    {
        $category = Categories::find($id);
        return view('admin.pages.createCate')->with('category',$category);
    }


    public function postCate(CateRequest $request)
    {
        $input = $request->all();        
        $cate = new categories;
        
        $cate->parentId = $input["parentId"];
        
        $cate->name = $input["name"];
      
        $cate->save();
        return redirect('admin/category/show');
    }

    public function createParent(){
        return view('admin.pages.createCateParent');
    }

    public function postCateParent(CateRequest $request)
    {
        $input = $request->all();        
        $cate = new categories;
        
        $cate->parentId = $input["parentId"];
        
        $cate->name = $input["name"];
      
        $cate->save();
        return redirect('/admin/category/show');
    }

    public function editCate($id)
    {
        $category = categories::find($id);
        return view('admin.pages.editCate')->with('category', $category);
    }

    public function updateCate(CateRequest $request)
    {
        $input = $request->all();
        $id = $request->input('id');        
        $category = categories::find($id);              
        $category->name = $request->input('name',50);     
        $category->save();
        return redirect('admin/category/show');
    }
    public function destroyCate($id)
    {
        $cate = Categories::find($id);
        
        $ca = Categories::all();
        foreach ($ca as $key => $value) {
           if($value->parentId == $cate->id)
           {
             
                CategoriesController::destroyCate($value->id);
                
           }
        }

        $cate->delete();
        return redirect('admin/category/show');
    }
    public function dequy($array,$parentId, $count)
    {

    $cate_html = '';
    foreach($array as $element)
    {
      if($element->parentId==$parentId)
      { 
       if($parentId == 0)
       {
        $count= '';
       }
        $cate_html .= '<tr>';
        $cate_html .= '<td>';
        $cate_html .= '</td>';
        $cate_html .= '<td>'.$count.$element->name;
        $cate_html .= '</td>';
        $cate_html .= '<td>'.$element->created_at;
        $cate_html .= '</td>';
        $cate_html .= '<td>'.$element->updated_at;
        $cate_html .= '</td>';
        $cate_html .= '<td>'."<a href= 'create/$element->id' class='btn btn-success'><i class='glyphicon glyphicon-plus'></i> Add".'</a>';
        $cate_html .= '</td>';
        $cate_html .= '<td>'."<a href= 'edit/$element->id' class='btn btn-primary'><i class='glyphicon glyphicon-plus'></i> Edit".'</a>';
        $cate_html .= '</td>';
        $cate_html .= '<td>'."<a href = 'delete/$element->id'  onclick = 'return check()'  class='btn btn-danger'><i class='glyphicon glyphicon-plus'></i> Delete".'</a>';
        $cate_html .= '</td>';    
        $cate_html .= CategoriesController::dequy($array,$element->id,$count.'//-- ');
        
      }
    }
    $cate_html .= '</tr>';
    return $cate_html;
  }
}