<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'emailCustomer' => 'required|email',
            'phoneCustomer' => 'required',
            'addressCustomer' => 'required',
            'address2Customer' => 'required',
            'cityCustomer' => 'required'
        ];
    }
    public function messages () {
        return [ 
            'firstname.required'     => ' ho la bat buoc',
            'lastname.required'       => 'ten la bat buoc',
            'emailCustomer.required'    => 'email la bat buoc',
            'emailCustomer.email'       => 'nhap dung dinh dang email',
            'phoneCustomer.required'      => 'so dien thoai la bat buoc',
            'addressCustomer.required'  => 'dia chi la bat buoc',
            'address2Customer.required' => 'dia chi thu hai la bat buoc',
            'cityCustomer.required'     => 'thanh pho la bat buoc'
        ];
    }
}
