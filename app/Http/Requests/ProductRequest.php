<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'categoryId' => 'required',
            'image' => 'required',
            'cost' =>'required',
            'quantity' => 'required'
        ];
    }
    public function messages () {
        return [ 
            'name.required'     =>  'ten san pham la bat buoc',
            'categoryId.required'    => 'Danh muc san pham la bat buoc',
            'image.required' =>'Hinh anh san pham la bat buoc',
            'cost.required' =>'Gia san pham la bat buoc',
            'quantity.required' =>'So luong san pham la bat buoc'
        ];
    }
}
