<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users,name',
            'avatar' => 'required',
            'email' => 'required|unique:users,email|email'
        ];
    }
    public function messages () {
        return [ 
            'name.required'     => ' Vui long nhap ten',
            'name.unique'       => 'Ten nay da ton tai',
            'avatar.required'   => ' Vui long them avatar',
            'email.required'    => 'Vui long nhap email',
            'email.email'       => 'nhap dung dinh dang email',
            'email.unique'      => 'email nay da ton tai'
        ];
    }
}
