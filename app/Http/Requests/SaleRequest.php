<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percent' => 'required|integer|min:0|max:100',
            'fromdate' => 'required|date_format:Y-m-d',
            'todate'    => 'required|after:fromdate|date_format:Y-m-d'
        ];
    }
    public function messages () {
        return [ 
            'percent.required' => ' Nhap % giá bạn muốn giảm',
            'percent.integer' => '% giá muốn giảm phải là một số nguyên',
            'percent.min'   => '% phải lớn hơn 0',
            'percent.max'   =>  '% phải nhỏ hơn 100',
            'fromdate.required' => 'Ngày bắt đầu sale là bắt buộc',
            'todate.required' => 'Ngày kết thúc sale là bắt buộc',
            'todate.after'     =>  'Thời gian kết thúc phải muộn hơn thời gian bắt đầu.'
        ];
    }
}
