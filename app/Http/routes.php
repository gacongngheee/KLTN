<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('home', 'homeController@index');
Route::get('loai-san-pham/{id}', 'homeController@loaisanpham');
Route::get('ProductDetail/{id}', 'homeController@ProductDetail');
Route::get('mua-hang/{id}','homeController@muahang');
Route::get('deleteCart/{id}','homeController@xoagiohang');
Route::get('yourcart','homeController@giohang');
Route::get('checkout','orderController@getcheckout');
Route::post('postcheckout','orderController@postcheckout');
Route::get('order/verify/{key_active}', [ // trang xac thuc don hang
    'as' => 'confirmation_path',
    'uses' => 'VerifyOrderController@confirm'
]);
Route::get('/SearchByCate','homeController@searchbycategory');
Route::get('/setsale/{id}','saleController@get');
Route::post('/postsetsale/{id}','saleController@set');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('admin/user/show', 'userController@index');
Route::get('admin/user/edit/{id}','userController@edit');
Route::get('admin/user/delete/{id}','userController@destroy');
Route::post('admin/user/update','userController@update');

Route::get('admin/category/show', 'categoriesController@getcate');
Route::get('admin/category/create/{id}', 'categoriesController@create');
Route::post('admin/category/post/{id}', 'categoriesController@postCate');
Route::get('admin/category/createParent', 'categoriesController@createParent');
Route::post('admin/category/postParent', 'categoriesController@postCateParent');
Route::get('admin/category/edit/{id}', 'categoriesController@editCate');
Route::post('admin/category/update/{id}', 'categoriesController@updateCate');
Route::get('admin/category/delete/{id}', 'categoriesController@destroyCate');

Route::get('admin/product/show', 'productController@showProduct');
Route::get('admin/product/create', 'productController@getCreate');
Route::post('admin/product/store', 'productController@store');
Route::get('admin/product/edit/{id}', 'productController@editProduct');
Route::post('admin/product/update/{id}','productController@updateProduct');
Route::get('admin/product/delImg/{id}','productController@getDelImg');
Route::get('admin/product/delete/{id}', 'productController@destroyProduct');

Route::group(['middleware' => 'auth'], function(){
		get('admin/user/show', 'userController@index');
		get('admin/user/edit/{id}','userController@edit');
		get('admin/user/delete/{id}','userController@destroy');
		post('admin/user/update','userController@update');

		get('admin/category/show', 'categoriesController@getcate');
		get('admin/category/create/{id}', 'categoriesController@create');
		post('admin/category/post/{id}', 'categoriesController@postCate');
		get('admin/category/createParent', 'categoriesController@createParent');
		post('admin/category/postParent', 'categoriesController@postCateParent');
		get('admin/category/edit/{id}', 'categoriesController@editCate');
		post('admin/category/update/{id}', 'categoriesController@updateCate');
		get('admin/category/delete/{id}', 'categoriesController@destroyCate');

		get('admin/product/show', 'productController@showProduct');
		get('admin/product/create', 'productController@getCreate');
		post('admin/product/store', 'productController@store');
		get('admin/product/edit/{id}', 'productController@editProduct');		
		post('admin/product/update/{id}','productController@updateProduct');
		get('admin/product/delImg/{id}','productController@getDelImg');
		get('admin/product/delete/{id}', 'productController@destroyProduct');
});


Route::get('test', 'homeController@test');