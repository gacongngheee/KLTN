<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','firstname','lastname', 'email', 'phone', 'address','address2','city','created_at', 'updated_at'];
    
    public function pOrder(){
    	return $this->hasMany('App\order');
    }
}
