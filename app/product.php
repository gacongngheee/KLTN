<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'categoryId', 'image','description', 'cost','quantity','sold', 'bestseller','created_at', 'updated_at'];
    
    public function pImage(){
    	return $this->hasMany('App\productImage');
    }
}
