<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productImage extends Model
{
    protected $table = 'productImage';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','product_Id','image', 'created_at', 'updated_at'];
    protected function product (){
    	return $this->belongTo('App\Product');
    }
    
}
