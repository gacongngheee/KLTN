<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderDetail extends Model
{
    protected $table = 'orderDetail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id','product_id', 'quantity', 'created_at', 'updated_at'];
    
    public function orderHas(){
        return $this->belongTo('App\Order');
    }
}
