<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','customer_id','key_active','actived','created_at', 'updated_at'];
    
    public function pOrderDetail(){
    	return $this->hasMany('App\orderDetail');
    }
    public function customerHas(){
        return $this->belongTo('App\customer');
    }
}
