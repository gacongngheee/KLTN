<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    protected $table = 'sale';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id','percent','fromdate','todate'];

    public function productHas(){
        return $this->belongTo('App\product');
    }
    public $timestamps = false;
}