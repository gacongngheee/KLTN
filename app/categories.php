<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Id','parentId', 'name', 'created_at', 'updated_at'];
    public function product(){
    	return $this->hasMany('App\product');
    }
}
